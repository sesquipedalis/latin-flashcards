#Colors: http://www.discoveryplayground.com/computer-programming-for-kids/rgb-colors/
#Color Generator: http://www.rapidtables.com/web/color/RGB_Color.htm
#Note: for converting in py2exe, you must write 'filename' python py2exe

#Add more WORDS and COLORS

import pygame, sys, os, random
from pygame.locals import *

Lavender=(230,230,250)
Blue=(100,149,237)
Black=(0,0,0)
Red=(178,34,34)
White=(255,250,240)
Green=(0,100,0)
Cerulean=(0,0,205)
Aqua=(102,205,170)
Coral=(255,127,80)
Pink=(255,105,180)
Lace=(255,182,193)
Rose=(255,228,225)
Purple=(160,32,240)
Turquoise=(64,224,208)
width,height=640,480
screen=pygame.display.set_mode((width,height))
pygame.display.set_caption('Latin Flashcards')
screen.fill(Lavender)
#February 16th:103 words
WORDS=[("amare:","to love"), ("stare:","to stand"), ("putare:","to think"), ("a:", "by,from"), ("aedificum:"," building"), ("ancilla:", "female servant"), 
("annos:", "year"), ("appropinquare:"," to approach"),
("auferre:","to steal"), ("audire:", "to hear"), ("avertere:", "to turn away, avert"), ("benignitas:"," kindness"), ("bibere:"," to drink"),
("cachinnare:"," to laugh loudly"), ("cantare:"," to sing"),("cupere:"," to capture"),("carota:"," carrot"),("celebrare:"," to celebrate"),
("cenare:"," to dine"),("cenaculum:"," apartment"),("cibus:"," food"),("civis:"," citizen"),("clamare:"," to shout"),
("commemorare:"," to mention"), ("conicere:"," to throw"),
("contendere:"," to hurry"),("cotidie:"," every day"),("revinere:"," to return"),
("delere:"," to destroy"), ("desistere:"," to stop"),("emere:"," to buy"),
("epistula:"," letter"),("equus:"," horse"),("excitare:"," to wake up"),("fortasse:"," perhaps"),
("identidem:"," repeatedly"),("laborare:"," to work"),
("lapillus:"," stone"),("laudere:"," to praise"),("ludere:"," to play"),("manere:"," to wait"),("mensa:"," table"),
("mittere:"," to send"),("percutere:"," to hit"), ("vindicare:"," to avenge"),
("carmen:"," song/poem"),("prensare:"," to grab"),("proximus/proxima:"," near"),
("canus:"," dog"),("recusare:"," to refuse"), ("adductus:"," terse, frowning"), ("permovere:"," to move deeply"), 
("constituere:"," to set up"), ("permotus:"," agitated, excited"), ("proficisci:"," to depart"),
("comparare: ","to prepare"), ("sementis", "sowing, planting"), ("ut:", "in order to"), ("frumentum:","grain, crops"),
("confirmare:", "to strengthen, develop"), ("biennis:", "lasting two years"), ("ducere:","to lead"), ("profectio:", "departure"),
("conficere:", "to make"),("suscipere:","to undertake"),("occupere:", "to seize"),("item:", "likewise, similarly"),
("tempus:","time"),("conari:","to attempt"), ("probare:", "to approve"), ("conatum:","attempt"),("propterea:","therefore"),
("quin:","without"), ("exercitus:", "infantry"),("conciliare:","to unite"), ("adducere:", "to lead away"), ("potiri:", "to obtain"),
("sperare:", "to hope"), ("pervinire:", "to arrive"), ("ulterior:","farther, higher"), ("imperare:", "to order, to command"),
("qui:","who, which"),("pons:", "bridge"), ("rescindere:","to cut down, to destroy"), ("ullus:","any"), ("locus:", "rank, position"),
("maleficium:", "crime, offense"), ("exercere:","to train, to drill"), ("pulsus:","stroke, heartbeat"),
("existamare:","to value, to judge"), ("reverti:","to return"), ("interea:","meanwhile"),("lacus:","lake, pond"),
("dividere:","to divide, to separate"), ("passus:","step, pace"),("disponere:","to dispose, to place"),
("castella:","castle"),("communire:","to strengthen, to fortify"),("venire:","to come, to arrive"), 
("ratis:","raft, boat"),("complus:","many"), ("vadum:","shallow place, stream"), ("interdiu:","in the daytime, by day"),
("perrumpere:","to break through"), ("telum:", "dart, spear"), ("opus:", "need, work")
("perrumpere:","to break through"), ("telum:", "dart, spear"), ("opus:", "need, work"), ("profugus:","fugitive"),
("condere:","to build"), ("puppis:","stern of a ship"), ("basiatio:","kiss"), ("deesse:","to lack, miss"),
("discedere:","to depart"), ("excedere:","to go out"), ("sustinere:","to hold upright"), ("concurrere:","to concur, to coincide"),
("confestim:","suddenly"),("ponere:","to place"), ("vallum:","wall"), ("comportare:","to transport"), ("turris:","tower"),
("coortus:","arisen"), ("fusilis:","molten, liquid"), ("coepisse:","to begin"), ("docere:","to teach"),
("exhaurire:","to draw out, to empty"),("niti:","to lean on"), ("recedere:","to retreat"), ("sol:","sun"),("eximius:","select, special"),
("privare:", "to deprive, to rob"), ("vereri:","to revere, to respect"),("famulus:","serving, servile"),
("decidere:","to detach, to cut"), ("coepisse:", "to have begun"), ("aliter:","otherwise, differently"),
("demere","to take away from, to subtract"),("viscus:","organs, entrails"),("vestis:","garment, clothing"),
("afferre:","to carry, to report"),("iaculari:","to throw (a javelin), to hurl"),("cadere:","to fall, to drop"),
("petra:","rock"),("pyram:","funeral pile"), ("construere:","to make, to construct"), ("donare:"," to present")]

COLORS=[Blue, Black, Red, Green, Cerulean, Aqua, Pink, Purple, Turquoise]



def main():
	
	width,height=640,480
	screen=pygame.display.set_mode((width,height))
	pygame.display.set_caption('Latin Flashcards')
	screen.fill(Lavender)
	pygame.init()
	TitleScreen()
	runGame()
	

def runGame():
	
	while 1:
	
		for event in pygame.event.get():
			if event.type==QUIT:
				sys.exit()
			if event.type==pygame.KEYDOWN and event.key==K_SPACE:
				next=True
				if next==True:
					Flashcard()
			elif event.type==pygame.KEYDOWN and event.key==K_RETURN:
				getword=True
				if getword==True:
					GetWord()
			if event.type==KEYDOWN and event.key==K_a:
				pygame.image.save(screen,"screenshot.png")		
	
				
				
		pygame.display.flip()	
		
def TitleScreen():
	titleFont=pygame.font.SysFont(None,22)
	titleSurf1=titleFont.render('Latin Flashcards', 1 , Black)
	titleSurf2=titleFont.render('Press the Spacebar to Continue.',1, Blue)

	screen.blit(titleSurf1,(230,200))
	screen.blit(titleSurf2,(180,250))
	

		
			
def Flashcard():
	pygame.display.flip()
	screen.fill(Lavender)
	font=pygame.font.SysFont(None,30)
	statement=font.render("Press Enter for a new word.",1, Blue)
	screen.blit(statement,(160,200))
	
def GetWord():
	font=pygame.font.SysFont(None,35)
	which_word=random.choice(range(len(WORDS)))
	word=WORDS[which_word][0]
	defi=WORDS[which_word][1]
	randword=font.render(word,1,White)
	randdef=font.render(defi,1,White)
	color=random.choice(COLORS)
	screen.fill(color)
	screen.blit(randword,(250,200))
	screen.blit(randdef,(245,250))
	

		
			
main()